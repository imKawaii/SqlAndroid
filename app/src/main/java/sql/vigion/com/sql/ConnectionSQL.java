package sql.vigion.com.sql;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by a48578 on 06-01-2016.
 */
public class ConnectionSQL {

    private String server = "10.1.26.60";  //ip ou url da maquina com SQLServer
    private String driverSQLServer = "net.sourceforge.jtds.jdbc.Driver"; //Connector
    private String database = "Escola";
    private String dbUserId = "tgpsi";
    private String dbPassword = "esferreira123";

    //Método devolve uma Connection pronta a usar
    @SuppressLint("NewApi")         //suprime warnning desta classe
    public Connection connect() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Connection conn = null;
        String ConnURL = null;
        try{
            Class.forName(driverSQLServer);
            ConnURL = "jdbc:jtds:sqlserver://" + server
                    + ";databaseName=" + database
                    + ";user=" + dbUserId
                    + ";password" + dbPassword + ";";
            conn = DriverManager.getConnection(ConnURL);
        } catch (SQLException ex) {
            Log.e("ERRO", ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Log.e("ERRO", ex.getMessage());
        } catch (Exception ex) {
            Log.e("ERRO", ex.getMessage());
        }
        return conn;
    }
}
